import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Tests for class DigitCounter")
public class DigitCounterTest {

    @Nested
    @DisplayName("Tests for method countDigit")
    class CountDigitTest {

        @Test
        public void countsDigitZeroTimes() {
            // given
            DigitCounter counter = new DigitCounter(123456);
            // when
            int nrOfDigits = counter.countDigit(7);
            //then
            assertEquals(0, nrOfDigits);
        }

        @Test
        public void countsDigitThreeTimes() {
            // given
            DigitCounter counter = new DigitCounter(-712374567);
            // when
            int nrOfDigits = counter.countDigit(7);
            //then
            assertEquals(3, nrOfDigits);
        }

        @Test
        public void doesNotChangeMemberWhenCountingDigits() {
            // given
            DigitCounter counter = new DigitCounter(-712374567);
            // when
            int nrOfDigits1stCall = counter.countDigit(7);
            int nrOfDigits2ndCall = counter.countDigit(7);

            //then
            assertEquals(nrOfDigits1stCall, nrOfDigits2ndCall);
        }

        @Test
        public void failToCountNonDigit() {
            // given
            DigitCounter counter = new DigitCounter(-712374567);
            // when
            int nrOfDigits = counter.countDigit(10);
            //then
            assertEquals(-1, nrOfDigits);
        }

        @Test
        public void failToCountNegativeDigit() {
            // given
            DigitCounter counter = new DigitCounter(712374567);
            // when
            int nrOfDigits = counter.countDigit(-1);
            //then
            assertEquals(-1, nrOfDigits);
        }
    }

    @Nested
    @DisplayName("Tests for method containsDigit")
    class ContainsDigitTest {

        @Test
        public void doesContainsDigit() {
            // given
            DigitCounter counter = new DigitCounter(-712374567);
            // when
            boolean doesContainDigit = counter.containsDigit(7);
            //then
            assertTrue(doesContainDigit);
        }

        @Test
        public void doesNotContainDigit() {
            // given
            DigitCounter counter = new DigitCounter(123456);
            // when
            boolean doesContainDigit = counter.containsDigit(7);
            //then
            assertFalse(doesContainDigit);
        }

    }

    @Nested
    @DisplayName("Tests for method getDigits")
    class GetDigitsTest {

        private int[] convertListToArray(ArrayList<Integer> digitList) {
            int[] digitArray = new int[digitList.size()];
            for (int i = 0; i < digitList.size(); i++) {
                digitArray[i] = digitList.get(i);
            }
            return digitArray;
        }

        @Test
        public void getDigitsOfNegativeNumberContainingSevenThreeTimes() {
            // given
            DigitCounter counter = new DigitCounter(-71637457);
            // when
            ArrayList<Integer> digits = counter.getDigits();
            //then
            int[] expectedDigits = {1, 3, 4, 5, 6, 7};
            assertArrayEquals(expectedDigits, convertListToArray(digits));
        }

        @Test
        public void doesNotChangeMemberWhenGettingDigitsSeveralTimes() {
            // given
            DigitCounter counter = new DigitCounter(-71637457);
            // when
            ArrayList<Integer> digits1stCall = counter.getDigits();
            ArrayList<Integer> digits2ndCall = counter.getDigits();
            //then
            assertArrayEquals(convertListToArray(digits1stCall), convertListToArray(digits2ndCall));
        }

    }

}
